import java.util.Scanner;

public class SimpleCalc {
    public static void main(String args[]) {
        Scanner scObj = new Scanner(System.in);
        double a, b, c = 0.0;

        System.out.print("Enter the first number \n");
        a = scObj.nextDouble();
        System.out.print("Enter the second number \n");
        b = scObj.nextDouble();
        System.out.print("Choose the operation  " +
                "\n1.Addition" +
                "\n2.Subtraction" +
                "\n3.Multiplication" +
                "\n4.Division" +
                "\nPlease enter the number of your choice \n");
        double operation = scObj.nextDouble();
        double addition = 1;
        double subtraction = 2;
        double multiplication = 3;
        double division = 4;
        if (operation == addition) {
            c = a + b;
            System.out.println(a + " + " + b + " = " + c);
        } else if (operation == subtraction) {
            c = a - b;
            System.out.println(a + " - " + b + " = " + c);
        } else if (operation == multiplication) {
            c = a * b;
            System.out.println(a + " * " + b + " = " + c);
        } else if (operation == division) {
            c = a / b;
            System.out.println(a + " / " + b + " = " + c);
        }
    }
}
